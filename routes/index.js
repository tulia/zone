const config = require("config"),
  express = require("express"),
  fs = require("fs-extra"),
  // isIp = require('is-ip'),
  qs = require("qs"),
  log = require("../lib/logger")("request", "req"),
  _ = require("lodash");

const siteController = require("../controllers/site.controller");
const adminController = require("../controllers/admin.controller");
const userController = require("../controllers/user.controller");

const router = express.Router();

const Gateway = require("../lib/gateway/" + config.get("GATEWAY.type")),
  path = require("path"),
  imageDataURI = require("image-data-uri");

let templateData;

let bg_images = fs.readdirSync(path.join(__dirname, "../public/images/bg")),
  img_idx = 0;








router.all("*", async (req, res, next) => {
  log.info(_.pick(req, "path", "method", "query", "body"));

  let bg_img = "/images/bg/" + _.sample(bg_images);
  // console.log(bg);

  let templateData = {
    //   host: `http://${internal_ip}:${port}`,
    bg_img,
    logo: "/images/logo.svg",
    /*await imageDataURI.encodeFromFile(
        path.join(__dirname, "../public/images/smiling-4654734_1280.jpg")
      ),await imageDataURI.encodeFromFile(
        path.join(__dirname, "../public/images/logo.svg")
      ),*/
    tunnel: req.app.settings.tunnel,

    config: Object.assign({}, config),
  };

  // console.log(req.app.settings);
  let zone = req.app.settings.zone;
  templateData.zone = zone;

  // console.log(zone);

  // get user ipGET
  let ip = res.locals.ip;
  templateData.ip = ip;

  //add query
  templateData.query = req.query;

  // functions
  templateData.f = {
    json: (o) => JSON.stringify(o, 0, 4),
    qs,
  };

  res.locals = templateData;

  // get device details
  let gateway = Gateway(zone.gw_ip);

  let dhcp_list = await gateway.get_dchp();

  let device = _.pick(_.find(dhcp_list, { ip }) || {}, "hostname", "mac", "ip");

  templateData.device = device;

  // flash data
  let error = req.flash("error");
  let info = req.flash("info");
  templateData.flash = null;

  if ((error && error.length )|| (info && info.length)) {
    templateData.flash = {
      error,
      info,
    };
  }

  //console.log(Object.keys(templateData));
  next();
});

router.route("/").get(siteController.showHome);

router.route("/reg/:ip").get(userController.regForm);

router.route("/user").post(userController.regDevice);

router.route("/admin/login").get(adminController.loginPage);
router.route("/admin/login").post(adminController.loginPost);

// dashboard
router
  .route("/admin/dashboard")
  .get(adminController.validateSession, adminController.showDashboard);
router
  .route("/admin/dashboard/add-user")
  .get(adminController.validateSession, adminController.showAddUser);
router
  .route("/admin/dashboard/add-user")
  .post(adminController.validateSession, adminController.addUser);

router.route("/reg-device").post(adminController.regDevice);

module.exports = router;

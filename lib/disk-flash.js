// Copyright 2021 Anthony Mugendi
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

let validate = require("aproba"),
  path = require("path"),
  IP = require("ip4");

const Cache = require("./cache"),
  cache = Cache("flash");

class Flash {
  constructor() {}

  set(type, msg) {
    let data = arrify(cache.get(type));
    // add flash message
    data.push(msg);
    // save as array
    cache.set(type, data);
  }

  read(type) {
    // console.log(this);
    let data = cache.get(type);
    cache.del(type);
    return data;
  }
}

// arrify
function arrify(o) {
  if (o === null || o === undefined) o = [];
  if (!Array.isArray(o)) o = [o];
  return o;
}

let flashCls = new Flash();

function flash(ip, key, msg) {

  validate("SSS|SS", arguments);

  // add ip to key
  key = `${ip}:${key}`;

  if (msg) {
    data = flashCls.set(key, msg);
    return true;
  } else {
    return flashCls.read(key);
  }
}

let mw = (req, res, next) => {

  req.flash = function(){

    let args = Array.from(arguments);

    // get IP if not already in locals
    let ip = res.locals.ip || IP.get(req);

    res.locals.ip = ip;

    args.unshift(ip);

    return flash.call(null, ...args)
  };

  if (typeof next == "function") {
    
    next();
  }
};




module.exports = mw;

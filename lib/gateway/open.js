// Copyright 2021 Anthony Mugendi
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const got = require("got"),
  _ = require("lodash");

class Gateway {
  constructor(gw_ip) {
    this.baseUrl = "http://" + gw_ip;
    this.url = this.baseUrl + "/goform/formJsonAjaxReq";

    this.get_dchp();
  }

  async request(url, payload, method = "post") {
    // console.log({ url, payload });
    let { body } = await got[method](url, {
      json: payload,
      responseType: "json",
    }).catch((err) => {
      return { body: null };
    });

    if (!body || body.status !== 0) {
      body = {
        status: -1,
        data: null,
      };
    }

    return body.data;
  }

  async get_dchp() {
    let self = this;
    if (!self) return null;
    // console.log(">>>>>", self.baseUrl);

    let payload = { action: "dhcp_list" };

    return await this.request(this.url, payload);
  }

  async get_wifi() {
    let self = this;
    if (!self) return null;
    // console.log(">>>>>", self.baseUrl);

    let payload = { action: "client_list" };

    return await this.request(this.url, payload);
  }

  async unblock_devices(ids) {
    // ensure strings
    ids = ids.map(String);

    let payload = {
      action: "set_macacl",
      data: { oper: "del", list: ids },
    };
    // console.log(payload);
    return await this.request(this.url, payload);
  }

  async block_device(mac, note = "") {
    let self = this;
    // mac needs to have no colons
    mac = mac.replace(/:/g, "");

    console.log({ mac });

    // check if device is already blocked
    let is_blocked = await self.this_blocked_device(mac);
    // console.log({is_blocked});

    if (is_blocked) {
      return {
        macmode: "deny",
        list: [is_blocked],
      };
    }

    // else do the actual block
    let payload = {
      action: "set_macacl",
      data: { mac, note, oper: "add", id: "_empty" },
    };

    return await this.request(this.url, payload);
  }

  async this_blocked_device(mac) {
    let self = this;

    mac = mac.replace(/:/g, "");

    // get blocked devices
    let resp = await self.get_blocked_devices();
    resp = resp || { list: [] };

    return _.find(resp.list, { mac });
  }

  async get_blocked_devices() {
    let payload = { action: "macacl_list" };
    return await this.request(this.url, payload);
  }

  async apply_changes() {
    let payload = { action: "set_macacl", data: { oper: "apply" } };
    return await this.request(this.url, payload);
  }
  
  async reboot() {
    // console.log("Reboot");
    let payload = { action: "do_reboot" };
    return await this.request(this.url, payload);
  }
}

module.exports = (ip) => {
  return new Gateway(ip);
};

// Copyright 2021 Anthony Mugendi
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const requestIp = require("request-ip"),
  ip6addr = require("ip6addr");

function get_ip(req) {
  let clientIp = requestIp.getClientIp(req);

  try {
    var addr = ip6addr.parse(clientIp);
    clientIp = addr.toString({ format: "v4" });
  } catch (error) {}

  return clientIp;
}

module.exports.get = get_ip;

module.exports.mw = (req, res, next)=>{

    let ip = get_ip(req);

    res.locals.ip = ip;

    if(typeof next == "function") next();

}

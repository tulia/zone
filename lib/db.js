// Copyright 2021 Anthony Mugendi
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Type 3: Persistent datastore with automatic loading
var Datastore = require("nedb"),
  path = require("path"),
  P = require("./promisify"),
  fs = require("fs-extra");

// db dir...
const dbDir = path.join(__dirname, "../db");

// ensure database dir
fs.ensureDirSync(dbDir);

class DB {
  constructor(dbPath) {
    this.db = new Datastore({
      filename: dbPath,
      autoload: true,
    });
  }

  ensureIndex() {
    let args = Array.from(arguments);
    return new Promise((resolve, reject) => {
      this.db.ensureIndex(
        ...args,
        resolve_reject(resolve, reject, "ensure-index")
      );
    });
  }

  insert() {
    let args = Array.from(arguments);
    return new Promise((resolve, reject) => {
      this.db.insert(...args, resolve_reject(resolve, reject, "insert"));
    });
  }

  find() {
    let args = Array.from(arguments);
    return new Promise((resolve, reject) => {
      this.db.find(...args, resolve_reject(resolve, reject, "find"));
    });
  }

  findOne() {
    let args = Array.from(arguments);
    return new Promise((resolve, reject) => {
      this.db.findOne(...args, resolve_reject(resolve, reject, "findOne"));
    });
  }

  update() {
    let args = Array.from(arguments);
    return new Promise((resolve, reject) => {
      this.db.update(...args, resolve_reject(resolve, reject, "update"));
    });
  }
}

function resolve_reject(resolve, reject, f) {
  return function (err, resp) {
    if (err) return reject(err);
    resolve(resp);
  };
}

// async start db & ensure indexes
(async function () {
  // console.log(db.devices);
  // set up indexes

  module.exports = {
    init: () => {
      db = {
        devices: new DB(path.join(dbDir, "devices.nedb")),
        zone: new DB(path.join(dbDir, "zone.nedb")),
      };
      
      db.devices.ensureIndex({ fieldName: "mac", unique: true });
      db.devices.ensureIndex({ fieldName: "blocked", unique: false });

      db.zone.ensureIndex({ fieldName: "needs_restart", unique: false });
      db.zone.ensureIndex({ fieldName: "_id", unique: true });
      return db;
    },
    reset: () => {
      // clear the database folder
      fs.emptyDirSync(dbDir);
    },
  };
})();

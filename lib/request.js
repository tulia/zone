// Copyright 2021 Anthony Mugendi
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const got = require('got');

class Request{
    constructor(){}

    async request(url, payload, method = "post") {
    
        // console.log({ url, payload });
        let { body } = await got[method](url, {
          json: payload,
          responseType: "json",
        }).catch((err) => {
          return { body: null };
        });
    
        return body;
      }


    async post(url, payload){
        return this.request(url, payload, "post");
    }

    async get(url, payload){
        return this.request(url, payload, "get");
    }

}


module.exports = new Request;
// Copyright 2021 Anthony Mugendi
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const fs = require("fs-extra"),
  flatCache = require("flat-cache"),
  path = require("path"),
  cacheDir = path.join(__dirname, "../cache");

fs.ensureDirSync(cacheDir);

class Cache {
  constructor(nameSpace) {
    let self = this;

    let cache = flatCache.load(nameSpace, cacheDir);

    this.cache = cache;

    this.rm = this.del;
  }

  set(key, val) {
    this.cache.setKey(key, val);
    this.cache.save();
    return true;
  }
  get(key) {
    return this.cache.getKey(key);
  }
  del(key) {
    return this.cache.removeKey(key);
  }
  
}

module.exports = (nameSpace) => {
  return new Cache(nameSpace);
};

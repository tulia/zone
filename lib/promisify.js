// Copyright 2021 Anthony Mugendi
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

function get_methods(obj){
    let properties = new Set();
    let currentObj = obj;
    do {
      Object.getOwnPropertyNames(currentObj).map((item) => properties.add(item));
    } while ((currentObj = Object.getPrototypeOf(currentObj)));
    return [...properties.keys()].filter(
      (item) => typeof obj[item] === "function"
    );
  };
  
  function promisify_all(cls) {
    // let o = {};
  
    let methods = get_methods(cls);
  
    for(let m of methods){
      cls["_"+m] = promisify(cls[m], cls);
    }
    
    return cls;
    
  }
  
  function promisify(func, ctx = null) {
    let f = function () {
      let args = Array.from(arguments);

      // console.log(args);
  
      return new Promise((resolve, reject) => {

        let cb = function (err, resp) {
          if (err) return reject(err);
          resolve(resp);
        };
  
        args.push(cb);
  
        func.call(ctx, ...args);

      });
    };
  
    return f;
  }

  
  module.exports = {
      promisify,
      promisify_all
  }
// Copyright 2021 mugz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const config = require("config"),
  path = require("path"),
  bunyan = require("bunyan"),
  bformat = require("bunyan-format"),
  formatOut = bformat({ outputMode: "short" }),
  fs = require("fs-extra");

const env = process.env.NODE_ENV;

module.exports = (name, type = "err") => {
  let logsDir = path.join(__dirname, "..", "logs", name);

  fs.ensureDirSync(logsDir);
  fs.emptyDir(logsDir);

  let log = bunyan.createLogger({
    name: config.get("APP.name") + ":" + name,
    streams: [
      //print logs in stdout while in dvt
      env == "development"
        ? {
            level: "info",
            stream: formatOut,
          }
        : //else dont print logs
          null,
      {
        level: "info",
        path: path.join(logsDir, "info.log"),
        type: "rotating-file", //rotate logs
        period: "1d", // monthly rotation
        count: 100, // keep 12 back copies
      },
      {
        level: "error",
        path: path.join(logsDir, "err.log"), // log ERROR and above to a file
        type: "rotating-file", //rotate logs
        period: "1d", // daily rotation
        count: 100,
      },
    ].filter((o) => o !== null),
  });

  return log;
};

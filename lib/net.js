// Copyright 2021 Anthony Mugendi
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const config = require("config"),
  log = require("./logger")("net");

const _ = require("lodash"),
  isOnline = require("is-online"),
  delay = require("delay"),
  P = require("./promisify"),
  network = P.promisify_all(require("../bin/network")),
  wifiName = require("wifi-name"),
  wifi = require("../bin/node-wifi"),
  got = require("got"),
  networkSpeed = require("./network-speed"),
  Gateway = require("../lib/gateway/" + config.get("GATEWAY.type"));

const ipInfoUrl = "https://ipinfo.io?token=" + config.get("IPINFO.token");

let ZONE;

// init  wifis
wifi.init({
  iface: null, // network interface, choose a random wifi interface if set to null
});

async function this_gateway() {
  let { gateway_ip } = await network._get_active_interface();
  return Gateway(gateway_ip);
}

async function block_device(device, note = "") {
  // get gateway Class
  let gateway = await this_gateway();

  return await gateway.block_device(device.mac, note);
}

async function unblock_devices(deviceIds, note = "") {
  // get gateway Class
  let gateway = await this_gateway();

  return await gateway.unblock_devices(deviceIds);
}
async function reboot_gw(){
  let gateway = await this_gateway();

  return await gateway.reboot();

}

async function apply_changes(){
  let gateway = await this_gateway();

  return await gateway.apply_changes();
}

async function is_this_device_blocked(mac){
  let gateway = await this_gateway();
  return await gateway.this_blocked_device(mac)
}

async function blocked_devices(mac){
  let gateway = await this_gateway();
  return await gateway.get_blocked_devices(mac)
}

async function get_connected_devices(removeSelf = true) {
  // get gateway Class
  let gateway = await this_gateway();

  let dhcp_list = await gateway.get_dchp(),
    wifi_list = await gateway.get_wifi(),
    devices = [],
    iface,
    device;

  /**
   * PSM:  Port State Machine
   *
   */

  if (removeSelf) {
    iface = await network._get_active_interface();
  }

  if (wifi_list && wifi_list.length) {
    for (let o of wifi_list) {
      // get device
      device = _.find(dhcp_list, { mac: o.mac });

      if (device && (!removeSelf || device.ip !== iface.ip_address)) {
        // console.log({device});

        o = _.merge(o, device);
        devices.push(o);
      }
    }
  }

  return devices;
}

async function ensure_online(returnSate=false) {
  let appOnline = await isOnline();

  if(returnSate) return appOnline;

  // wait for app to get online
  while (!appOnline) {
    log.info("Waiting for internet connection...");
    await delay(3000);
    // check again
    appOnline = await isOnline();
  }
}

async function find_gw_wifi(ssid, retries = 0) {
  if (retries) {
    log.info(
      `Attempting to find ${ssid} WiFi. ${
        retries ? retries + " retires..." : ""
      } `
    );
  }

  let networks = await wifi.scan(),
    gwWifi = _.find(networks, { ssid }),
    max_retries = config.get("GATEWAY.retry_connect");

  //   console.log("gwWifi", gwWifi && gwWifi.ssid);

  if (!gwWifi && retries < max_retries) {
    gwWifi = _.find(networks, { ssid });
    retries++;

    await delay(5000);

    gwWifi = await find_gw_wifi(ssid, retries);
  }

  return gwWifi;
}



async function get_wifi_ssid() {
  return await wifiName().catch((err) => null);
}

async function ensure_gw_connected() {
  // get wifi ssid
  wifiSSID = await get_wifi_ssid();

  let retries =0;

  // if ssid is not what we expected
  while (wifiSSID !== ZONE.wifi.ssid) {
    // check that wifi is found
    await find_gw_wifi(ZONE.wifi.ssid);
    // reconnect...
    log.info(`Changing WiFi connection to "${ZONE.wifi.ssid}"...` + (retries?` ${retries} retries...`:''));

    // try connect
    await wifi.connect(ZONE.wifi).catch((err) => null);

    wifiSSID = await get_wifi_ssid();

    await delay(3000);
    retries++;
  }

  return wifiSSID;
}

async function get_ip_data() {
  log.info("Fetching IP Data...");

  return await got
    .get(ipInfoUrl, {
      timeout: 5000,
      responseType: "json",
    })
    .then((resp) => resp.body)
    .catch((err) => null);
}

async function get_gw_data() {
  let wifiSSID = await ensure_gw_connected();

  log.info("Getting ZONE...");

  let ipData = await get_ip_data();

  // retry getting IP data
  while (!ipData) {
    await delay(1000);
    ipData = await get_ip_data();
  }

  // console.log(ipData);
  let iface = await network._get_active_interface();
  let gwWifi = await find_gw_wifi(ZONE.wifi.ssid);

  let data = Object.assign(
    {},
    _.omit(ZONE, "wifi"),
    {
      port: config.get("APP.port"),
      country: ipData.country,
      provider: ipData.org,
      public_ip: ipData.ip,
      private_ip: iface.ip_address,
      gw_ip: iface.gateway_ip,
      mac: iface.mac_address,
      wifi_name: wifiSSID,
    },
    _.pick(gwWifi, "frequency", "mode")
  );

  return data;
  object;
}

async function get_net_speed() {
  let gwWifi = await find_gw_wifi(ZONE.wifi.ssid);
  // console.log(gwWifi);
  let speed = await networkSpeed.download();
  let data = _.merge({}, speed, _.pick(gwWifi, "signal_level", "quality"));
  return data;
}

module.exports = function (zone) {
  ZONE = zone;
  return {
    // ensure_online,
    // find_gw_wifi,
    ensure_gw_connected,
    // get_ip_data,
    get_gw_data,
    get_net_speed,
    get_connected_devices,
    block_device,
    unblock_devices,
    reboot_gw,
    apply_changes,
    ensure_online,
    is_this_device_blocked,
    blocked_devices
  };
};

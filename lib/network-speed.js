// Copyright 2021 Anthony Mugendi
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const NetworkSpeed = require("network-speed"); // ES5
const testNetworkSpeed = new NetworkSpeed();

async function download() {
  // content
  const baseUrl = "https://eu.httpbin.org/stream-bytes/500000",
    fileSizeInBytes = 500000;

  let speed = await testNetworkSpeed.checkDownloadSpeed(
    baseUrl,
    fileSizeInBytes
  );

  for (let i in speed) {
    speed[i] = Number(speed[i]);
  }

  return speed;
}

async function upload() {
  // post data
  const options = {
      hostname: "dlptest.com",
      port: 80,
      path: "/http-post/",
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
    },
    fileSizeInBytes = 2000000;

  let speed = await testNetworkSpeed.checkUploadSpeed(options, fileSizeInBytes);

  for (let i in speed) {
    speed[i] = Number(speed[i]);
  }

  return speed;
}

module.exports = {
  download,
  upload,
};

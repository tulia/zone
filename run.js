// Copyright 2021 mugz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const forever = require("forever-monitor"),
  path = require("path"),
  delay = require("delay"),
  fs = require("fs"),
  log = require("./lib/logger")("runner");

//forever run scripts...
function forever_run(scriptData) {

  let scriptDir = path.join(__dirname, "./scripts"),
    script = path.join(scriptDir, scriptData.script),
    maxRestarts=1000;

  // console.log({ script });

  if (fs.existsSync(script)) {
    let opts = {
      max: maxRestarts,
      silent: false,
      watch: true,
      // watch: script,
      watchDirectory: [scriptDir].concat((scriptData.watchDirs || [])),
      args: [],
    };


    log.info(`Starting ${scriptData.script}...`);

    // 
    let child = new forever.Monitor(script, opts);

    child.on("watch:restart", function (info) {
      log.error("Restarting script because " + info.file + " changed");
    });

    child.on("restart", function () {
      log.error(`Forever restarting ${scriptData.script} for ${child.times} time`);
    });

    child.on("exit:code", function (code) {
      log.error(`Forever detected ${scriptData.script}  exited with code ${code}`);
    });

    child.on("exit", function () {
      log.info(`Server has exited after ${maxRestarts} restarts`);
    });

    child.start();
  }
}

// 
(async () => {

  // 
  let scripts = [
    {
      script: "server.js",
      watchDirs: ["views", "routes", "controllers"].map((f) =>
        path.join(__dirname, f)
      ),
      delay: 2000,
    },
    // {
    //   script: "tunnel.js",
    // }
  ];

  // 
  for (let s of scripts) {
    forever_run(s);
    // if we need to delay
    if (s.delay) {
      log.info(`Waiting ${s.delay} ms...`);
      await delay(s.delay);
    }
  }

})();

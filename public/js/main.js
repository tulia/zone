// Copyright 2021 mugz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

$(function () {
  let hasError = false,
    phoneInput = $("#phone");

  // if we have a phone input
  if (phoneInput.length) {
    phone_number_ctrl();

    let form = phoneInput.parents("form");

    if (form) {
      form.submit(function (e) {
        if (hasError) {
          hasError = false;
          return false;
        }
      });
    }
  }

  async function phone_number_ctrl() {
    let errorMsg = $("#error-msg"),
      validMsg = $("#valid-msg"),
      data = phoneInput.data();

    let iti = window.intlTelInput(phoneInput[0], {
      // any initialisation options go here
      autoPlaceholder: "polite",
      placeholderNumberType: "MOBILE",
      initialCountry: data.country || "KE",
      customPlaceholder: function (
        selectedCountryPlaceholder,
        selectedCountryData
      ) {
        return "e.g. " + selectedCountryPlaceholder;
      },
      utilsScript: "/vendor/intl-tel-input/js/utils.js",
    });

    // setTimeout(() => {
    //     phoneInput.val("0721644677")
    // }, 100);

    function reset() {
      hasError = false;
      errorMsg.addClass("hidden");
      validMsg.addClass("hidden");
    }

    // on blur: validate
    phoneInput
      .blur(function () {
        reset();
        let phone = phoneInput.val();

        if (phone.trim()) {
          //   validate number
          if (iti.isValidNumber()) {
            validMsg.removeClass("hide");
          } else {
            phoneInput.addClass("error");
            errorMsg.html("Incorrect Phone Number");
            errorMsg.removeClass("hidden");
            hasError = true;
          }
        }
      })
      .keydown(reset);
  }
});

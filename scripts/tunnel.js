// Copyright 2021 mugz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const config = require("config"),
  port = config.get("APP.port"),
  fs = require("fs-extra"),
  path = require("path"),
  localtunnel = require("localtunnel"),
  log = require("../lib/logger")("tunnel");

// start localtunnel if not started
const tunnelSettingDir = path.join(__dirname, "../tunnel"),
  tunnelDataFile = path.join(tunnelSettingDir, "settings.json"),
  tunnelData = fs.existsSync(tunnelDataFile)
    ? fs.readJsonSync(tunnelDataFile)
    : {},
  tunnelAlive = false;

// ensure directory
fs.ensureDirSync(tunnelSettingDir);

log.info("Starting local-tunnel...");

// start localtunnel
localtunnel({ port })
  .then((tunnel) => {
    let data = {
      opts: tunnel.opts,
      clientId: tunnel.clientId,
      url: tunnel.url,
    };

    log.info(`Tunnel established: ${tunnel.url}`);

    // save localtunnel data
    fs.writeJsonSync(tunnelDataFile, data);
  })
  .catch((err) => console.error(err.message));

// Copyright 2021 Anthony Mugendi
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const config = require("config"),
  got = require("got"),
  _ = require("lodash"),
  delay = require("delay"),
  DB = require("../lib/db"),
  { diff } = require("just-diff");

const TuliaServer = config.get("SERVER.host"),
  URLObj = new URL(TuliaServer),
  ZONE = config.get("ZONE");

const {
  get_gw_data,
  get_net_speed,
  ensure_gw_connected,
  get_connected_devices,
  block_device,
  unblock_devices,
  reboot_gw,
  apply_changes,
  ensure_online,
  is_this_device_blocked,
  blocked_devices,
} = require("../lib/net")(ZONE);

let log = require("../lib/logger")("server");

// start with clean db
// DB.reset();
const db = DB.init();

function is_diff(obj1, obj2) {
  // ensure objects
  obj1 = _.isObject(obj1) ? obj1 : {};
  obj2 = _.isObject(obj2) ? obj2 : {};

  return diff(obj1, obj2).length > 0;
}

// Register Async function
async function register() {
  // get gateway data
  let payload = await get_gw_data();

  // add path
  URLObj.pathname = "/register-zone";
  let url = URLObj.href;

  log.info("Registering zone...");

  // register with server
  const { body } = await got.post(url, {
    json: payload,
    responseType: "json",
  });

  // console.log({ url, body });

  // console.log(body);
  return body;
}

// Fetch Hosts
async function fetch_hosts(zone_id) {
  log.info("Fetching hosts...");

  URLObj.pathname = "/zone-hosts/" + zone_id;
  let url = URLObj.href;

  const { body } = await got.get(url, {
    // json: payload,
    responseType: "json",
  });

  // console.log({ url, body });
}

// CRON FUNCTIONS;
async function report_network_speed(zone) {
  try {
    // get net speed
    let payload = await get_net_speed();

    URLObj.pathname = "/zone-speed/" + zone.id;
    let url = URLObj.href;

    const { body } = await got.post(url, {
      json: payload,
      responseType: "json",
    });

    // report zone speed
    // console.log({ body });
  } catch (error) {
    console.log(error);
  }
}

async function ensure_gw_is_connected() {
  let start = Date.now();
  let wifiSSID = await ensure_gw_connected();
}

async function report_connected_devices() {
  // get devices
  let devices = await get_connected_devices();

  // if there are devices
  if (devices.length === 0) return;

  for (let device of devices) {
    await update_device_status(device);
  }
}

async function report_connection_status() {
  // console.log("HERE");

  let devices = await get_connected_devices();

  let connected_devices = _.map(devices, "mac");

  // console.log({ connected_devices });
  // if we have connected devices, report this status to server
  if (connected_devices.length) {
    URLObj.pathname = "/zone-connection-status/" + ZONE._id;
    let url = URLObj.href;

    const { body } = await got.post(url, {
      json: { devices: connected_devices },
      responseType: "json",
    });

    // console.log({body});
  } else {
    // console.log("No connected devices...");
  }
}

async function check_idle_status() {
  log.info("Checking idle status...");

  URLObj.pathname = "/zone-connection-status/" + ZONE._id;
  let url = URLObj.href;

  const { body } = await got.get(url, {
      // json: {devices:connected_devices},
      responseType: "json",
    }),
    zoneIdle = body.code == config.get("HTTP_CODES.zone_offline");

  // console.log({ zoneIdle });

  // if zone is offline, see if we need to apply changes
  if (zoneIdle) {
    // if zone has some restarts to do
    let where = _.pick(ZONE, "_id");
    let zoneData = await db.zone.findOne(where);

    // If zone has changes to apply
    if (zoneData && zoneData.has_changes_to_apply) {
      log.info(`Zone idle. Applying Changes...`);

      await apply_changes();

      // update has_changes_to_apply = 0
      let doc = { has_changes_to_apply: 0 };
      await db.zone.update(where, { $set: doc });
    }
  }
}

async function update_device_status(device) {
  let where = _.pick(device, "mac");

  // check if device is registered
  let device_status = await get_device_status(device),
    reg_status = {
      status: device_status.message,
      status_code: device_status.code,
    };

  let saved_device = await db.devices.findOne(where);

  // console.log(device_status);

  // if not saved
  if (!saved_device) {
    // add status as unregistered
    device = _.merge(device, reg_status);
    device.blocked = false;
    device.blocked_time = 0;

    // insert to database
    saved_device = await db.devices.insert(device);
  }
  // update if reg status has changed
  else if (saved_device.status !== reg_status.status) {
    // if reg status has changed
    let doc = _.merge(_.omit(device, "mac"), reg_status);
    await db.devices.update(where, { $set: doc }, {});
  }

  // console.log({saved_device});
  let deviceBlocked = await is_blocked_device(device);

  // if not registered
  if (
    saved_device.status_code ==
      config.get("HTTP_CODES.device_not_registered") &&
    (await !deviceBlocked)
  ) {
    // check if device grace time has passed
    if (device.time > config.get("DEVICES.grace_time")) {
      log.info(`Blocking device : ${device.mac}`);

      // Block device
      let resp = await block_device(device, device_status.message);

      // console.log(resp);

      // indicate that device has been blocked
      await db.devices.update(
        where,
        { $set: { blocked: true, blocked_time: Date.now() } },
        {}
      );

      // blocked
      // change reboot status
      await set_zone_has_changes();
    }

    // console.log(device);
  }

  // console.log(device.hostname, (device.time)>config.get("DEVICES.grace_time"));
  // console.log({ saved_device , device});
}

async function is_blocked_device(device) {
  let blocked = await is_this_device_blocked(device.mac);
  return blocked && blocked.mac ? true : false;
}

async function set_zone_has_changes() {
  let where = _.pick(ZONE, "_id"),
    doc = {
      ...ZONE,
      has_changes_to_apply: 1,
    },
    exists = await db.zone.findOne(where);

  // if status has changed
  // console.log({exists});

  if (is_diff(exists, doc)) {
    // upsert
    db.zone.update(where, doc, { upsert: true });
  }
}

async function get_device_status(device) {
  URLObj.pathname = "/zone-device-status/" + device.mac;
  let url = URLObj.href;

  const { body } = await got.get(url, {
    // json: payload,
    responseType: "json",
  });

  return body;
}

async function unblock_devices_after_sinbin() {
  // get all blocked
  let now = Date.now(),
    blockedDevices = await blocked_devices(),
    // find devices blocked for more than "sinbin_period"
    blockedSince = now - config.get("DEVICES.sinbin_time"),
    dbBlockedDevices = await db.devices.find(
      {
        blocked: true,
        blocked_time: { $lt: blockedSince },
      },
      { mac: 1, blocked_time: 1 }
    );

  // console.log(config.get("DEVICES.sinbin_time"), { blockedSince, dbBlockedDevices });

  // if we have devices to unblock
  if (dbBlockedDevices.length === 0) return;

  // unblock
  if (blockedDevices && blockedDevices.list.length > 0) {
    // map with correct id as per gateway
    let devicesToUnblock = dbBlockedDevices
        .map((o) => {
          return _.find(blockedDevices.list, { mac: o.mac.replace(/:/g, "") });
        })
        .filter((o) => o && o.id),
      ids = _.map(devicesToUnblock, "id");

    // if we have ids to unblock
    if (ids.length) {
      // return
      log.info(`Unblocking the devices: ${ids.join(", ")}`);

      // unblock
      let resp = await unblock_devices(ids);
      // change reboot status
      await set_zone_has_changes();
      console.log(resp);
    }
  }

  // console.log(">>>>>");
  // console.log(dbBlockedDevices);

  // update block status
  // update block time
  let doc = {
      blocked: false,
      blocked_time: 0,
    },
    _ids = _.map(dbBlockedDevices, "_id");

  // console.log({ _ids });

  let resp = await db.devices.update({ _id: { $in: _ids } }, { $set: doc });
  console.log({ resp });
}

function tick() {
  // console.log('h');
}

// use cron to keep checking & update network speeds
// console.log(config.get("CRON"));
let cronFuncs = {
  tick,
  report_network_speed,
  // ensure_gw_is_connected,
  report_connected_devices,
  report_connection_status,
  check_idle_status,
  unblock_devices_after_sinbin,
};

function run_cron(zone) {
  log = require("../lib/logger")("cron");
  log.info(`Running cron for Zone: ${zone.id}`);

  const CronJob = require("cron").CronJob,
    humanToCron = require("human-to-cron");

  //
  let cronExp,
    cronConfig = config.get("CRON"),
    cronRunning = {},
    attemptingGWReconnection = false,
    onlineStatus;

  for (let cronFunc in cronConfig) {
    cronExp = humanToCron(cronConfig[cronFunc]);

    cronRunning[cronFunc] = false;

    new CronJob(
      cronExp,
      async () => {
        // ensure is online

        // ensure gateway is connected always.....
        if (!attemptingGWReconnection) {
          attemptingGWReconnection = true;
          await ensure_gw_is_connected();
          attemptingGWReconnection = false;
        }

        onlineStatus = await ensure_online(true);

        // only run if function and connected to GW.
        if (
          !cronRunning[cronFunc] &&
          "function" == typeof cronFuncs[cronFunc] &&
          onlineStatus
        ) {
          // log.info(`Cron func: ${cronFunc}...`);
          cronRunning[cronFunc] = true;
          await cronFuncs[cronFunc](zone);
          cronRunning[cronFunc] = false;
        }
      },
      null,
      true,
      "America/Los_Angeles"
    ).start();
  }
}

module.exports = {
  register,
  fetch_hosts,
  run_cron,
};

// Copyright 2021 mugz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const config = require('config');

const onlineSync = require("./online-sync"),
  // {ensure_online} = require("../lib/net"),
  delay = require("delay"),
  log = require("../lib/logger")("server");

// start.....
(async () => {
  // Ensure is online
  // await ensure_online();

  try {
    // sync with online server
    let resp = await onlineSync.register();

    if (resp.code == 200) {
      // we are good...
      // start server
      await start_server(resp.zone);
      // request host-list

      let hosts = await onlineSync.fetch_hosts(resp.zone.id);

      // console.log(hosts);

      // start cron...
      onlineSync.run_cron(resp.zone);
    }
  } catch (error) {
    console.log(error);
  }
})();

// function to start server
async function start_server(zone) {
  // constants
  const fs = require("fs-extra"),
    path = require("path"),
    express = require("express"),
    helmet = require("helmet"),
    // bodyParser = require("body-parser"),
    app = express(),
    flash = require("../lib/disk-flash"),
    IP = require("ip4"),
    forceServer = require("force-server-start");

  // set security HTTP headers
  app.use(
    helmet({
      contentSecurityPolicy: false,
    })
  );

  app.use(IP.mw);

  app.use(flash);

  //decode body
  app.use(express.urlencoded({ extended: true }));
  app.use(express.json());

  // static folder
  app.use(express.static("./public"));

  //views
  app.set("views", "./views");
  // engine
  app.set("view engine", "pug");

  app.set("zone", zone);

  if (app.get("env") === "development") {
    app.locals.pretty = true;
  }

  // v1 api routes
  app.use("/", require("../routes"));

  // run server using server authenticated port

  forceServer(app, zone.port)
    .then((resp) => {
      log.info(resp);
    })
    .catch(console.error);

  return zone;
}

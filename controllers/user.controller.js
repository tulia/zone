// Copyright 2021 mugz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// const internalIp = require("internal-ip"),
const config = require("config");

let templateData;

let regDevice = async (req, res, next) => {
  console.log(req.body);

  // register a users uid and IP

  res.json(Object.assign(req.body))
};


let regForm = async (req, res, next) => {
  const ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
  res.render("pages/reg-form",{ip:req.params.ip});
  // res.json(Object.assign(req.query, {ip}))
};

module.exports = {
  regDevice,
  regForm
};

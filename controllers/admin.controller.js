// Copyright 2021 mugz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// const internalIp = require("internal-ip"),
const config = require("config"),
  got = require("got"),
  fs = require("fs-extra"),
  path = require("path"),
  Cache = require("../lib/cache"),
  request = require("../lib/request");

const sessCache = Cache("sess");

async function get_session_id() {

  let sessionId = sessCache.get(cachePath, key);


  return null;
}

const TuliaServer = config.get("SERVER.host"),
  URLObj = new URL(TuliaServer);

let loginPage = async (req, res, next) => {
  let zone = req.app.settings.zone;

  // register a users uid and IP
  res.render("pages/login", {});
};

let loginPost = async (req, res, next) => {
  let zone = req.app.settings.zone;

  URLObj.pathname = "/zone-validate/" + zone.id;
  let url = URLObj.href;

  // console.log(url);
  const body = await request.post(url, req.body);

  // console.log(body);

  if (body && body.code == 200) {
    sessCache.set(key, body.sess);

    res.redirect("/admin/dashboard");

    return;
  }

  // register a users uid and IP
  res.render("pages/login", {});
};

let validateSession = async (req, res, next) => {
  try {
    let zone = req.app.settings.zone;

    let sess = await get_session_id();
    let payload = {
      sess,
    };

    URLObj.pathname = "/zone-session-validate/" + zone.id;
    let url = URLObj.href;

    // console.log(url);
    const body = await request.post(url, payload);

    if (body && body.code == 200 && body.sess == sess) {
      res.locals.sess = sess;
      res.locals.zone_data = body.zone_data;

      // passthru
      next();
    } else {
      // delete all sessions
      await sessCache.del(key);

      res.redirect("/admin/login");
    }
  } catch (error) {
    console.log(error);
  }
};

let showDashboard = async (req, res, next) => {
  let zone = req.app.settings.zone;
  // register a users uid and IP
  res.render("pages/dashboard", {});
};

let showAddUser = async (req, res, next) => {
  let zone = req.app.settings.zone;

  // console.log(zone);
  // register a users uid and IP
  res.render("pages/dashboard/add-user", {});
};

let addUser = async (req, res, next) => {
  let zone = req.app.settings.zone;
  let sess = res.locals.sess;
  let payload = {
    user: req.body,
    sess,
  };

  URLObj.pathname = "/zone-add-user/" + zone.id;
  let url = URLObj.href;

  // console.log(url);
  const body = await request.post(url, payload);

  // register a users uid and IP
  res.redirect("/admin/dashboard/add-user");
};

let regDevice = async (req, res, next) => {
  let zone = req.app.settings.zone;
  let sess = res.locals.sess;

  URLObj.pathname = "/zone-reg-device/" + zone.id;
  let url = URLObj.href;

  // console.log(url);
  const body = await request.post(url, req.body);

  // set flash messages
  if (body.code == 200) {
    req.flash("info", body.message);
    // remove device from ban lists
    
  } else {
    req.flash("error", body.message);
  }

  res.redirect("/");
};

module.exports = {
  loginPage,
  loginPost,
  validateSession,
  showDashboard,
  showAddUser,
  addUser,
  regDevice,
};
